import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {UserModel as User} from './user.model';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class UserService{

	private urlProvided: string = 'http://jsonplaceholder.typicode.com/users';

	constructor(private http: Http){ }
	
	private users: User[];

	getUsers() : Promise<User[]>{
		
		// if users are exist
		if(this.users)
			return Promise.resolve(this.users);
				
		return this.http.get(this.urlProvided).toPromise()
			.then((response:Response) => response.json())
			.then((userList: any[]) => {
				let users = userList.map(user => {
					return {
						id: user.id,
        	            name: user.name,
        	            email: user.email,
        	            address: [user.address.city, user.address.street, 
							user.address.suite, user.address.zipcode].join(' '),
        	            phone: user.phone,
        	            website: user.website,
        	            companyName: user.company.name
					}
				});

				this.users = users;

				return this.users;
			});		
	}
}