import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ng2-bootstrap';
import { AppComponent } from './components/app.component';
import { UserDetailsComponent } from './components/user-details.component';
import { UsersListComponent } from './components/users-list.component';
import {Routes, RouterModule} from '@angular/router';
import { UserService } from './user.service';

const appRoutes: Routes =[
    { path: 'users', component: UsersListComponent},
    { path: '**', redirectTo: '/users' },
    { path: 'user/:id', component: UserDetailsComponent,  outlet: 'popup'}
];

@NgModule({
  declarations: [
    AppComponent, UserDetailsComponent, UsersListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
