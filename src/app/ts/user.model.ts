export class UserModel{
    id: number;   
    name: string;
    email: string;
    address: string;
    phone: string;
    website: string;
    companyName: string;
}