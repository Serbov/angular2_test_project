import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { UsersListComponent } from './users-list.component';


describe('UsersListComponent (inline template)', () => {

  let comp:    UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;
  let de:      DebugElement;
  let element:      HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersListComponent ], // declare the test component
    })
    .compileComponents();  // compile template and css

    fixture = TestBed.createComponent(UsersListComponent);

    comp = fixture.componentInstance; // UsersListComponent test instance
   
    // query for the title <h1> by CSS element selector
    de = fixture.debugElement.query(By.css('td'));
   element = de.nativeElement; 
  });

  it('should display original title', () => {
    console.log(de);
   //  console.log('element');
    //fixture.detectChanges();
    //expect(el.textContent).toContain('gg');
  });

  /*it('should display a different test title', () => {
   // comp.title = 'Test Title';
    fixture.detectChanges();
    expect(el.textContent).toContain('Test Title');
  });
*/

});
