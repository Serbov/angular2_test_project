import { Component, ViewChild } from '@angular/core';
import { UserService } from './../user.service';
import { UserModel as User } from './../user.model';

@Component({
  selector: 'users-list',
  templateUrl: './../../templates/users-list.component.html',
  styleUrls: ['./../../style/users-list.component.css'],
  providers: []
})
export class UsersListComponent {
  
  users: User[]=[];

  constructor(private userService: UserService){}

  ngOnInit(){     
    this.userService.getUsers()
      .then(
          users => this.users=users,
          error => {console.log(error);}
      );
  }
}
