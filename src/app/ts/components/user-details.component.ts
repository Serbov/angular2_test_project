import { Component, ViewChild} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';  
import { ModalDirective } from 'ng2-bootstrap/modal';
import { UserService } from './../user.service';
import { UserModel as User } from './../user.model';

@Component({
    selector: 'user-details',
    templateUrl: './../../templates/user-details.component.html',
    styleUrls: ['./../../style/user-details.component.css'],
})

export class UserDetailsComponent {
    
  @ViewChild('popup') public popup:ModalDirective;

  id: number;
  
  constructor(private router: Router, private activateRoute: ActivatedRoute, private userService: UserService){
        this.id = +activateRoute.snapshot.params['id'];
  }

  currentUser: User;

  ngAfterViewInit(){
    
    this.userService.getUsers()
      .then(
          users => {            
            this.currentUser = users.find(user => user.id === this.id);        
          }
      )
      .then(
          data => this.openPopup(),
          error => this.closeComponent()
        );
  }

  openPopup(){
      this.popup.show();
      this.popup.onHide.subscribe($event => this.closeComponent());   
  }

  closeComponent(){
    this.router.navigate(['', { outlets: { popup: null } }]);
  }

}